import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddRestoComponent } from './add-resto/add-resto.component';
import { UpdateRestoComponent } from './update-resto/update-resto.component';
import { ListComponent } from './list/list.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonService } from './common.service';
import { ReactiveFormsModule } from '@angular/forms';
import { DummyComponent } from './dummy/dummy.component';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AddRestoComponent,
    UpdateRestoComponent,
    ListComponent,
    LoginComponent,
    RegisterComponent,
    DummyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers:  [ CommonService],
  bootstrap: [DummyComponent],
  // exports:[
  //           AppComponent,
  //         AddRestoComponent,
  //         UpdateRestoComponent,
  //         ListComponent,
  //         LoginComponent,
  //         RegisterComponent
  //       ]
})
export class AppModule { }
