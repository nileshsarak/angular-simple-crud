import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddRestoComponent } from './add-resto/add-resto.component';
import { DummyComponent } from './dummy/dummy.component';
import { ListComponent } from './list/list.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UpdateRestoComponent } from './update-resto/update-resto.component';

const routes: Routes = [
  { path: '',  
   redirectTo: '/login', 
   pathMatch: 'full'
   },
  { 
    path : 'add',
    component : AddRestoComponent
  },
  { 
    path : 'update/:id',
    component : UpdateRestoComponent
  },
  { 
    path : 'list',
    component : ListComponent
  },
  { 
    path : 'login',
    component : LoginComponent
  },
  { 
    path : 'register',
    component : RegisterComponent
  },
  { 
    path : 'dummy',
    component : DummyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  
})
export class AppRoutingModule { }
