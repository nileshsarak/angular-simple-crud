import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  collectionList: any;
  alert :boolean=false;
  all: any;

  constructor(private common : CommonService) { }

  ngOnInit(): void {
    this.common.getRstoList().subscribe((res)=>{
      this.collectionList = res;
      //console.log("data=>",this.collectionList);
    })
  }

  deleteResto(id:any){
    console.log("button click",id)
    this.common.deleteResto(id).subscribe((res)=>{
      this.alert = true;
    })

  }

  closeAlert(){
    this.alert=false;
  
  }
 


}
