import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-update-resto',
  templateUrl: './update-resto.component.html',
  styleUrls: ['./update-resto.component.css']
})
export class UpdateRestoComponent implements OnInit {
  alert: boolean = false;
  updateRestoForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl('')
  })
  constructor( private route : ActivatedRoute,private common : CommonService ) { }

  ngOnInit(): void {

    this.common.getCurrentData(this.route.snapshot.params.id).subscribe((result:any)=>{
      this.updateRestoForm = new FormGroup({
        name: new FormControl(result['name']),
        address: new FormControl(result['address']),
        email: new FormControl(result['email'])
      })
    })  
  }
  updateResto(){
    this.common.updateResto(this.route.snapshot.params.id,this.updateRestoForm.value).subscribe((resp)=>{
      console.log("data Updated SUccefully",resp);
      this.alert=true;
    })

  }
  closeAlert(){
    this.alert=false;

  }
}
