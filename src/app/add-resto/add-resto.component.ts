import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CommonService } from '../common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-resto',
  templateUrl: './add-resto.component.html',
  styleUrls: ['./add-resto.component.css']
})
export class AddRestoComponent implements OnInit {

  alert: boolean = false;
  addRestoForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl('')
  })
  allUsers: any;
  constructor(private common: CommonService, private router: Router) { }

  ngOnInit(): void {
    this.getAllData();
  }

  createResto() {
    // console.log('form value',this.addRestoForm.value);
    this.common.addResto(this.addRestoForm.value).subscribe((rep) => {
      this.alert = true;
      this.addRestoForm.reset();

    })
    // this.router.navigateByUrl('/list');
  }

  getAllData() {
    this.common.getRstoList().subscribe((res) => {
      this.allUsers = res;
    })
  }
  closeAlert() {
    this.alert = false;

  }

}
