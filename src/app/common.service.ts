import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  url = 'http://localhost:3000/resto';
  regUrl = ' http://localhost:3000/users'

  constructor(private http: HttpClient) { }

  getRstoList() {
    return this.http.get(this.url);

  }

  addResto(data: any) {
    return this.http.post(this.url, data)

  }

  getCurrentData(id: any){
    return this.http.get(`${this.url}/${id}`);

  }

  updateResto(id:any,data:any){
    return this.http.put(`${this.url}/${id}`,data);
  }

  deleteResto(id:any){
    return this.http.delete(`${this.url}/${id}`);
  }

  createUser(data:any){
    return this.http.post(this.regUrl,data);

  }

}
