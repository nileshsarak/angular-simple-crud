import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  createUserForm = new FormGroup({
    name : new FormControl(''),
    email : new FormControl(''),
    password : new FormControl('')
   
    
  })
  constructor( private common : CommonService) { }

  ngOnInit(): void {
  }

  createNewUser(){
    this.common.createUser(this.createUserForm.value).subscribe((resp)=>{
      console.log( "user Data==>", resp);
    })
  }
}
